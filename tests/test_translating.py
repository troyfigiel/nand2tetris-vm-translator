import pathlib

import pytest

import vmtranslator


@pytest.fixture()
def vm_dir(tmp_path):
    vm_dir = tmp_path / "contains_vm"
    vm_dir.mkdir()
    return vm_dir


@pytest.fixture()
def vm_file(vm_dir):
    return vm_dir / "file.vm"


@pytest.fixture()
def asm_file(vm_dir):
    return vm_dir / "file.asm"


@pytest.fixture(
    params=[
        "SimpleAdd",
        "StackTest",
        "StaticTest",
        "PointerTest",
        "BasicTest",
    ],
)
def fixture_contents(vm_dir, request):
    fixtures_dir = pathlib.PosixPath("tests") / "fixtures"
    vm_content = (fixtures_dir / f"{request.param}.vm").read_text()
    asm_content = (fixtures_dir / f"{request.param}.asm").read_text()
    return vm_content, asm_content


@pytest.fixture()
def fixture_dir():
    return pathlib.PosixPath("tests") / "fixtures"


def test_translate_fixtures(vm_file, asm_file, fixture_contents):
    vm_content, asm_content = fixture_contents
    vm_file.write_text(vm_content)

    vmtranslator.call(vm_file.as_posix())

    assert asm_file.read_text() == asm_content
