import io

import pytest

from vmtranslator.preprocessing import preprocess


@pytest.mark.parametrize(
    ("input_program", "expected_lines"),
    (
        ("", []),
        ("push constant 10", ["push constant 10"]),
        (
            "push constant 10\npush constant 20",
            ["push constant 10", "push constant 20"],
        ),
        (
            "push constant 10 // Inline comment\npush constant 20",
            ["push constant 10", "push constant 20"],
        ),
        (
            "// Comment\npush constant 10 // Inline comment\npush constant 20",
            ["push constant 10", "push constant 20"],
        ),
    ),
)
def test_preprocess(input_program, expected_lines):
    actual_lines = list(preprocess(io.StringIO(input_program)))

    assert actual_lines == expected_lines
