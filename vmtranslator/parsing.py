from typing import Literal
from pydantic import NonNegativeInt, PositiveInt, validate_arguments

INIT_STACK = [
    "@256",
    "D=A",
    "@SP",
    "M=D",
]

_GENERAL_PURPOSE_REGISTERS = [
    "@R13",
    "@R14",
    "@R15",
]


@validate_arguments
def parse(line_number: PositiveInt, line: str) -> list[str]:
    # TODO: This could be simplified a lot, if we would have multiple
    # dispatch on enums. Is this possible?
    dispatch_stack_operation = {
        ("push", "constant"): _push_constant,
        ("push", "static"): _push_static,
        ("push", "pointer"): _push_pointer,
        ("push", "this"): _push_from_named_memory("@THIS"),
        ("push", "that"): _push_from_named_memory("@THAT"),
        ("push", "local"): _push_from_named_memory("@LCL"),
        ("push", "argument"): _push_from_named_memory("@ARG"),
        ("push", "temp"): _push_from_named_memory("@TEMP"),
        ("pop", "static"): _pop_static,
        ("pop", "pointer"): _pop_pointer,
        ("pop", "this"): _pop_to_named_memory("@THIS"),
        ("pop", "that"): _pop_to_named_memory("@THAT"),
        ("pop", "local"): _pop_to_named_memory("@LCL"),
        ("pop", "argument"): _pop_to_named_memory("@ARG"),
        ("pop", "temp"): _pop_to_named_memory("@TEMP"),
    }

    dispatch_microcode = {
        "add": _binary("D=D+A"),
        "sub": _binary("D=A-D"),
        "and": _binary("D=D&A"),
        "or": _binary("D=D|A"),
        "neg": _unary("D=-D"),
        "not": _unary("D=!D"),
        "eq": _comparison("JEQ", line_number),
        "lt": _comparison("JGT", line_number),
        "gt": _comparison("JLT", line_number),
    }

    comment = [f"// {line}"]
    match line.split():
        case [command, segment, value]:
            operation = dispatch_stack_operation.get((command, segment))
            if operation is not None:
                return comment + operation(value)
        case [command]:
            microcode = dispatch_microcode.get(command)
            if microcode is not None:
                return comment + microcode

    raise ValueError(f"Could not parse line {line}.")


@validate_arguments
def _push_constant(value: NonNegativeInt) -> list:
    return [f"@{value}", "D=A"] + _PUSH_D


@validate_arguments
def _push_static(value: PositiveInt) -> list:
    return [f"@static.{value}", "D=M"] + _PUSH_D


@validate_arguments
def _push_pointer(value: Literal["0", "1"]) -> list:
    return [
        {"0": "@THIS", "1": "@THAT"}[value],
        "D=D",
    ] + _PUSH_D


def _push_from_named_memory(segment: str):
    initial_data_register = "D=M"
    if segment == "@TEMP":
        initial_data_register = "D=A"

    @validate_arguments
    def push(value: NonNegativeInt):
        return [segment, initial_data_register, f"@{value}", "A=D+A", "D=M"] + _PUSH_D

    return push


@validate_arguments
def _pop_static(value: NonNegativeInt) -> list:
    return _POP_D + [f"@static.{value}", "M=D"]


@validate_arguments
def _pop_pointer(value: Literal["0", "1"]) -> list:
    return _POP_D + [
        {"0": "@THIS", "1": "@THAT"}[value],
        "M=D",
    ]


def _pop_to_named_memory(segment):
    initial_data_register = "D=M"
    if segment == "@TEMP":
        initial_data_register = "D=A"

    @validate_arguments
    def pop(value: NonNegativeInt):
        return (
            [
                segment,
                initial_data_register,
                f"@{value}",
                "D=D+A",
                _GENERAL_PURPOSE_REGISTERS[0],
                "M=D",
            ]
            + _POP_D
            + [_GENERAL_PURPOSE_REGISTERS[0], "A=M", "M=D"]
        )

    return pop


@validate_arguments
def _comparison(value, line_number: PositiveInt) -> list:
    return (
        _POP_D
        + _POP_A
        + [
            "D=D-A",
            f"@jump_{line_number}",
            f"D;{value}",
            "D=0",
            f"@next_{line_number}",
            "0;JMP",
            f"(jump_{line_number})",
            "D=-1",
            f"(next_{line_number})",
        ]
        + _PUSH_D
    )


_POP_D = [
    "@SP",
    "M=M-1",
    "A=M",
    "D=M",
]

_POP_A = [
    "@SP",
    "M=M-1",
    "A=M",
    "A=M",
]

_PUSH_D = [
    "@SP",
    "M=M+1",
    "A=M-1",
    "M=D",
]


@validate_arguments
def _binary(value) -> list:
    return _POP_D + _POP_A + [value] + _PUSH_D


@validate_arguments
def _unary(value) -> list:
    return _POP_D + [value] + _PUSH_D
