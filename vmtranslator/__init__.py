from vmtranslator.translating import call, translate

__all__ = ["call", "translate"]
