import pathlib
import sys
from typing import TextIO

from vmtranslator.preprocessing import preprocess
from vmtranslator.parsing import parse, INIT_STACK


def call(file_name: str | None = None) -> None:
    if file_name is None:
        file_name = sys.argv[1]

    vm_file = pathlib.Path(file_name)

    with vm_file.open() as f:
        asm_content = translate(f)

    vm_file.with_suffix(".asm").write_text(asm_content)


def translate(file_object: TextIO) -> str:
    translation = INIT_STACK.copy()

    for line_number, line in enumerate(preprocess(file_object), start=1):
        translation += parse(line_number, line)

    return "\n".join(translation) + "\n"
