from typing import TextIO, Iterable


COMMENT_START = "//"


def preprocess(file_object: TextIO) -> Iterable[str]:
    with file_object as f:
        for line in f:
            if COMMENT_START in line:
                line = line[: line.find(COMMENT_START)]

            line = line.strip()
            if line:
                yield line
