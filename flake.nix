{
  inputs = {
    devenv.url = "github:cachix/devenv";
    nixpkgs.url = "github:nixos/nixpkgs";
    poetry2nix = {
      url = "github:nix-community/poetry2nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { flake-parts, ... }@inputs:
    flake-parts.lib.mkFlake { inherit inputs; } {
      imports = [ inputs.devenv.flakeModule ];
      systems = [ "x86_64-linux" ];

      perSystem = { system, ... }:
        let pkgs = inputs.nixpkgs.legacyPackages.${system};
        in {
          devenv.shells.default = {
            languages.python = {
              enable = true;
              package = pkgs.poetry2nix.mkPoetryEnv {
                python = pkgs.python311;
                projectDir = ./.;
              };
            };

            packages = [
              # We add poetry separately, as otherwise devenv will
              # create a venv.
              pkgs.poetry
              pkgs.python3Packages.jedi-language-server
            ];

            enterShell = ''
              export PYTHONPATH="$DEVENV_ROOT:$PYTHONPATH"
            '';

            pre-commit.hooks = {
              black.enable = true;
              nixfmt.enable = true;
              ruff.enable = true;
            };
          };
        };
    };
}
